@author Vivekanand Koya, William Tran

ASM
line 2) Add the immediate value to the register value
line 3) Return to the caller.

line 5) Return values to go in r1.
line 6) Restore register r0.
line 7) Branch causes a branch to a target address.

line 11) Branch with link calls subroutine at PC relative address and sets LR to the return address.
line 12)  return values to go into r4.
line 13) Branch with link calls subroutine at PC relative address and sets LR to the return address.
line 14)  return values to go into r1
line 15) Branch with link calls subroutine at PC relative address and sets LR to the return address.
line 16) Branch with link calls subroutine at PC relative address and sets LR to the return address.

C
line 6) Adds two integers taken from the arguments together

line 11) Prints the integer taken as an argument to the system

line 15) Sets a variable to a random integer
line 16) Sets a second variable to a random integer
line 17) Calls a different function with the two variables as an integer and saves it to a new variable
line 18) Calls a different function with the newly saved variable to print it to the system
![c90f262b7e71f09ecf15f1a6b037f37b.png](c90f262b7e71f09ecf15f1a6b037f37b.png)
